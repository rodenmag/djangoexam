from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserForm
from django.contrib.auth.decorators import login_required
from django.views import generic
from .models import *
from django.contrib.auth.models import Group, User

def register(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            User = form.save()
            #group = Group.objects.get(form.cleaned_data['group'])
            User.groups.add(form.cleaned_data['group'] )
            messages.success(request, 'Account created!')
            return redirect('login')
    else:
        form = UserForm()
    return render(request, 'users/register.html', {'form': form})

@login_required
def profile(request):
    return render(request, 'users/profile.html')



class ProfileUpdateView(generic.UpdateView):
    model = Profile
    fields = ['banned']
