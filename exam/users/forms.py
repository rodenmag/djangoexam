from django import forms
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import UserCreationForm

class UserForm(UserCreationForm):
    email = forms.EmailField()
    group = forms.ModelChoiceField(queryset=Group.objects.all(), required=True)
    class Meta:
        model = User
        fields = ['username', 'group', 'email', 'password1', 'password2']

    #def __init__(self, *args, **kw):
    #    super(UserForm, self).__init__(*args, **kw)
    #    self.fields['group'].queryset=Group.objects.filter(user=self.instance.id)