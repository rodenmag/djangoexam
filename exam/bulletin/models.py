from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

class Boards(models.Model):
    name = models.CharField(max_length=30)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    created_on = models.DateField(auto_now=True)
    position_in_home = models.PositiveIntegerField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('board-details', kwargs={'pk': self.pk})

class Threads(models.Model):
    title = models.CharField(max_length=50)
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE)
    posted_on = models.DateField(auto_now=True)
    is_locked = models.BooleanField()
    boards = models.ForeignKey(Boards, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('board-home')


class Post(models.Model):
    message = models.CharField(max_length=100)
    posted_on = models.DateField(auto_now=True)
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE)
    threads = models.ForeignKey(Threads, on_delete=models.CASCADE)

    def __str__(self):
        return self.message

    def get_absolute_url(self):
        return reverse('board-home')

