from django.urls import path
from .views import *

urlpatterns = [
    path('', BoardsListView.as_view(), name='board-home'),
    path('<int:pk>/', BoardsDetailView.as_view(), name='board-details'),
    path('board/new/', BoardsCreateView.as_view(), name='board-create'),
    path('board/<int:pk>/delete/', BoardsDeleteView.as_view(), name='board-delete'),
    path('thread/<int:pk>/', ThreadsDetailView.as_view(), name='thread-details'),
    path('post/<int:pk>/new/', PostCreateView.as_view(), name='post-create'),
    path('thread/<int:pk>/new/', ThreadCreateView.as_view(), name='thread-create'),
    path('thread/<int:pk>/lock/', ThreadUpdateView.as_view(), name='thread-update'),
]
