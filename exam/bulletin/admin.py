from django.contrib import admin
from .models import *

admin.site.register(Boards)
admin.site.register(Threads)
admin.site.register(Post)