from django.views import generic
from .models import *
from django.shortcuts import get_object_or_404

class BoardsListView(generic.ListView):
    model = Boards
    template_name = 'bulletin/home.html'
    context_object_name = 'board_list'
    ordering = ['position_in_home']

class BoardsCreateView(generic.CreateView):
    model = Boards
    fields = ['name', 'position_in_home']

    def form_valid(self, form):
        form.instance.creator = self.request.user
        return super(BoardsCreateView, self).form_valid(form)

class BoardsDeleteView(generic.DeleteView):
    model = Boards
    success_url = '/'

class BoardsDetailView(generic.DetailView):
    model = Boards
    template_name = 'bulletin/boards_details.html'

class ThreadsDetailView(generic.DetailView):
    model = Threads
    template_name = 'bulletin/thread_details.html'
    context_object_name = 'thread_list'

class ThreadCreateView(generic.CreateView):
    model = Threads
    fields = ['title', 'is_locked']

    def form_valid(self, form):
        form.instance.posted_by = self.request.user
        form.instance.boards = Boards.objects.get(pk=self.kwargs.get('pk'))
        return super(ThreadCreateView, self).form_valid(form)

class ThreadUpdateView(generic.UpdateView):
    model = Threads
    fields = ['is_locked']
    def form_valid(self, form):
        form.instance.posted_by = self.request.user
        #form.instance.boards = Boards.objects.get(pk=self.kwargs.get('pk'))
        return super(ThreadUpdateView, self).form_valid(form)

class PostCreateView(generic.CreateView):
    model = Post
    fields = ['message']

    def form_valid(self, form):
        form.instance.posted_by = self.request.user
        form.instance.threads = Threads.objects.get(pk=self.kwargs.get('pk'))
        return super(PostCreateView, self).form_valid(form)


class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'bulletin/home.html'
    context_object_name = "post_list"
